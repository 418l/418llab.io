VERSION=$(git log --pretty=format:'%cd' --date=short -1)
REV=$(git log --pretty=format:'%h' --abbrev-commit -1)
TAINTED=$(if [[ -n $(git status --porcelain 2> /dev/null | tail -n1) ]]; then echo -n "+"; else echo -n ""; fi)

export JEKYLL_418L_VERSION="${VERSION}r${REV}${TAINTED}"

