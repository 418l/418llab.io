# 418l.pw website

Source code for [418l.pw](https://418l.pw/).

## Build requirements

### Linux
OS: Ubuntu 16.10

Install dependencies:

~~~shell
sudo apt install build-essential ruby ruby-dev nodejs devscripts debhelper
sudo gem install bundler
~~~

## Build 418l.pw

Run the following commands:

~~~shell
git clone https://gitlab.com/dtrsan/418l.pw
cd 418l.pw
bundle update jekyll
make build
~~~

Open http://127.0.0.1:4000 inside your browser.


## Build Debian package

Run the following commands

~~~shell
source init-build-env.sh
make dpkg
~~~~

## Known issues

None

