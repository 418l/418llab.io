---
layout: page
title: Debian packages
tags:
  - deb
sections:
  - memento
---
* TOC
{:toc}

## Introduction

Minimal steps to create a Debian package.

## Dependencies

Install dependencies.
~~~shell
apt-get install dh-make build-essential devscripts fakeroot debootstrap pbuilder
~~~

## Create base Debian files

Create necessary debian files which from which you will create a Debian package.
~~~shell
export DEBFULLNAME="Domagoj Tršan"
export DEBEMAIL=domagoj.trsan@gmail.com

REV=$(git log --pretty=format:'%h' --abbrev-commit -1)
VERSION=$(git log --pretty=format:'%cd' --date=short -1)
TAINTED=$(if [[ -n $(git status --porcelain 2> /dev/null | tail -n1) ]]; then echo -n "+"; else echo -n ""; fi)
BUILD_NUMBER=1

dh_make -e domagoj.trsan@gmail.com -p 418l.pw_${VERSION}r${REV}.${BUILD_NUMBER}${TAINTED} -i -n
~~~

Customize debian/\* files to suit your needs.

## Build Debian package

To build unsigned Debian package run:
~~~shell
debuild --prepend-path /usr/local/bin -i -us -uc -b
~~~

deb, build and changes files will be created in the parent directory.

## Requirements

Tested on Ubuntu 17.04

## Links
 - [Debian New Maintainers' Guide - Chapter 6. Building the package][debian-deb-docs]
 - [Pragmatic Debian packaging][deb-pkg]
 - [How to create a Debian package][deb-how-to]

[debian-deb-docs]: https://www.debian.org/doc/manuals/maint-guide/build.en.html
[deb-pkg]: https://vincent.bernat.im/en/blog/2016-pragmatic-debian-packaging.html
[deb-how-to]: https://santi-bassett.blogspot.ie/2014/07/how-to-create-debian-package.html

