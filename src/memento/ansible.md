---
layout: page
title: Ansible
tags:
  - ansible
sections:
  - memento
---
* TOC
{:toc}

## Introduction

Ansibe HOW-TOs, tips & tricks.

## Installation

Installing ansible inside isolated virtual environment gives you flexibility to have multiple independed installation of ansible.

### Manual steps
1. Install dependencies
    ~~~ shell
    sudo apt-get install virtualenv libpython2.7-dev libssl-dev
    ~~~

2. Setup a virtual enviroment for ansible
    ~~~ shell
    virtualenv ansible
    source ./ansible/bin/activate
    pip install ansible
    ~~~

    If you see the following error
    > The executable python2 (from --python=python2) does not exist

    re-run `virtualenv` command with `--python=python2.7` parameter:

### Automated steps
1. Copy `ansible-init.sh` script to your machine.
2. Run `ansible-init.sh` script on your machine.

## Host setup

### Manual steps
Execute the following commands on a host.

1. Install python 2.7
~~~ shell
sudo apt-get install python2.7
~~~

2. Add ansible user
~~~ shell
sudo adduser admin
~~~

3. Edit sudo security policies appending the following line with `sudo visudo` or create a new file with it in `/etc/sudoers.d`.

    > admin ALL=(ALL) NOPASSWD: ALL

4. Copy SSH public key to `~admin/.ssh/authorized_keys`

### Automated steps
1. Copy `ansible-host-init.sh` script and a SSH public key to a host.
2. Run `ansible-host-init.sh` script on the host.

## Scripts
1. [`ansible-init.sh`][ansible-init]
2. [`ansible-host-init.sh`][ansible-host-init]

## Requirements

Tested on Ubuntu 16.04 LTS

## Links

 - [ansible.com][ansible]
 - [ansible docs][ansible-docs]
 - [ansible examples][ansible-examples]

[ansible-init]: https://gitlab.com/dtrsan/scrapbook/blob/master/bin/ansible-init.sh
[ansible-host-init]: https://gitlab.com/dtrsan/scrapbook/blob/master/bin/ansible-host-init.sh
[ansible]: https://www.ansible.com/
[ansible-docs]: https://docs.ansible.com/ansible/
[ansible-examples]: https://github.com/ansible/ansible-examples

