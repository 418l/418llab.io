---
layout: page
title: nix cli
tags:
  - linux
  - unix
  - cli
  - shell
sections:
  - memento
---
* TOC
{:toc}

## Introduction

Various shell commands...

### Top 10 CPU processes
~~~shell
ps -eo pcpu,size,pid,user,args | sort -k 1 -n -r | head -10
~~~

### Top 10 MEM processes
~~~shell
ps -eo pcpu,size,pid,user,args | sort -k 2 -n -r | head -10
~~~

### Top 10 largest directories
~~~shell
ls -A | \grep -v -e '^\.\.$' |xargs -i du -ks {} |sort -rn |head -11 | awk '{print $2}' | xargs -i du -hs {}
~~~
~~~shell
du –max-depth 1 -h .|sort -rh|head -12
~~~

### sed one-liners
[Useful one-line scripts for sed (Unix stream editor)](/files/sed1line.txt)

