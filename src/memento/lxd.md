---
layout: page
title: LXD
tags:
  - lxd
sections:
  - memento
---
* TOC
{:toc}

## Introduction

LXD configuration

## Domain names support
### container &lt;-&gt; container

_My memory is a little cloudy about this so it may be incorrect or it's supported out of the box._

To enable domain name support run on host
~~~shell
lxc network set lxdbr0 dns.domain lxc
~~~

Get address
~~~
% lxc network show lxdbr0 | \grep 'ipv4.address' | \grep -E -o '[0-9]+.[0-9]+.[0-9]+.[0-9]+'
10.10.10.1
~~~

Verify DNS response
~~~
% dig @10.10.10.1 kafka.lxc

; <<>> DiG 9.10.3-P4-Ubuntu <<>> @10.10.10.1 kafka.lxc
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 28110
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;kafka.lxc.                     IN      A

;; ANSWER SECTION:
kafka.lxc.              0       IN      A       10.10.10.172

;; Query time: 0 msec
;; SERVER: 10.10.10.1#53(10.10.10.1)
;; WHEN: Fri Apr 21 18:58:10 IST 2017
;; MSG SIZE  rcvd: 54
~~~

Connect to a container and ping other containters via domain name (&lt;container&gt;.lxc)
~~~
% lxc exec dev-418lpw bash
root@dev-418lpw:~# ping -c1 kafka.lxc
PING kafka.lxc (10.10.10.172) 56(84) bytes of data.
64 bytes from kafka.lxc (10.10.10.172): icmp_seq=1 ttl=64 time=0.064 ms

--- kafka.lxc ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.064/0.064/0.064/0.000 ms
~~~

### host &lt;-&gt; container

Quick, dirty and temporary solution which will not be preserved after reboot.

Prepend `nameserver 10.10.10.1` to `/etc/resolv.conf.`

## Running GUI app inside a container

Start a container
~~~shell
lxc launch ubuntu:17.04 cn1 -c environment.DISPLAY=${DISPLAY}
lxc config device add cn1 x disk source=/tmp/.X11-unix/ path=/tmp/.X11-unix/
xhost -local:
~~~

Start an app
~~~shell
lxc exec cn1 firefox
~~~

## Direct user UID/GID mappings

Add correct mappings for a user to file `/etc/subuid`
~~~
user:$(id):$CNT_USER_ID
~~~

Don't forget to restart LXD.
~~~shell
sudo systemctl restart lxd
~~~

~~~shell
lxc config device add c1 disk-name disk path=/home/ubuntu/dest-dir source=/home/dtrsan/source-dir
printf "uid $(id -u) 1000\ngid $(id -g) 1000" | lxc config set c1 raw.idmap
lxc restart c1
~~~

You might need to restart the container too.

## Requirements

Partially tested on
 - Ubuntu 17.04 / 17.10.

## Links

 - [Enable GUI apps in a container][enable-gui-apps]
 - [Custom User Mappings in LXD Container][custom-user-mappings]

[enable-gui-apps]: https://www.reddit.com/r/LXD/wiki/container_configuration_tips/enable_gui_apps_in_a_container
[custom-user-mappings]: https://insights.ubuntu.com/2017/06/15/custom-user-mappings-in-lxd-containers

