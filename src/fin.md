---
layout: page
title: Fin
order: 100
sections:
  - menu
permalink: /fin/
---
# News
* [Dividend.com](http://www.dividend.com/) - dividend stocks - ratings, news, and opinion
* [ETF Databse](http://etfdb.com) - comprehensive guide to ETFs
* [ETF.com](https://www.etf.com/) - ETF tools, ratings, news
* [etfinfo.com](https://www.etfinfo.com/en/home/) - platform for ETFs
* [Irish Times Fund Lite](http://funds.irishtimes.com/) - list of funds performance
* [Stoxx](https://www.stoxx.com/) - indices / news / research / resources

# Forums / Information / Opinion
* [Askaboutmoney](http://askaboutmoney.com/) - the consumer forum
* [Boards.ie](https://www.boards.ie) - Ireland's online forum (has topics about personal finance and taxation)
* [Boogleheads.org](https://www.bogleheads.org/) - investing advice inspired by John Bogle
* [EU Investing](https://www.bogleheads.org/wiki/EU_investing) - this page deals specifically with investing in the European Union
* [Reddit Personal Finance (EU)](https://www.reddit.com/r/eupersonalfinance/wiki/basics) - common topics
* [Reddit Personal Finance (UK)](https://www.reddit.com/r/ukpersonalfinance/wiki/lumpsuminvestment) - common topics
* [Reddit Personal Finance (US)](https://www.reddit.com/r/personalfinance/wiki/commontopics) - common topics
* [Reddit Personal Finance Investing](https://www.reddit.com/r/personalfinance/wiki/investing)
* [Reddit Personal Finance Reading List](https://www.reddit.com/r/personalfinance/wiki/readinglist)

## Ireland
* [Irish Tax Institute](http://taxinstitute.ie/)
* [PWC Tax Facts](https://www.pwc.ie/taxfacts)
* [Taxation in the Republic of Ireland](https://en.wikipedia.org/wiki/Taxation_in_the_Republic_of_Ireland)
* [Taxworld Ireland](https://www.taxworld.ie/) - Irish tax law legislation online

# Cryptocurrencies
* [Bitcoin Bubble Burst](https://bitcoinbubbleburst.github.io) - alerts about Bitcoin
* [Bitcoin Fork Monitor](https://bch.btcforkmonitor.info/) - fork monitor for Bitcoin
* [BitInfoCharts](https://bitinfocharts.com/) - cryptocurrency statistics
* [Blockchain Graveyard](https://magoo.github.io/Blockchain-Graveyard/) - cryptocurrencies breaches/attacks
* [Blockchain](https://blockchain.info) - bitcoin cryptocurrency wallet and block explorer service
* [CoinCheckup](https://coincheckup.com/) - cryptocoin analysis and investments stats
* [CoinMarketCap](https://coinmarketcap.com/) - cryptocurrency market capitalizations
* [Crypto Maps](http://cryptomaps.org/) - cryptocurrency market state visualization
* [Cryptowatch](https://cryptowat.ch/) - live Bitcoin price charts
* [Etherchain](https://etherchain.org/) - ethereum blockchain explorer
* [EtherScan](https://etherscan.io/) - ethereum blockchain explorer, API and analytics platform
* [fork.lol](https://fork.lol/) - mining profitability based on fees, exchange rate and mining difficulty
* [Map of Coins](http://mapofcoins.com/) - list and descriptions of all cryptocurrencies
* [OhMyCo.in](http://ohmyco.in/) - trading charts

# Cryptocurrencies - Exchanges
* [bisq](https://bisq.network/) - p2p / decentralized exchange network
* [BlockExplorer News](https://blockexplorer.com/news/top-25-cryptocurrency-exchanges-2017/) - Top 25 Cryptocurrency Exchanges of 2017
* [Coinbase](https://www.coinbase.com/) - digital broker exchanges of Bitcoin (₿), Ethereum (Ξ), Litecoin (Ł) and other digital assets with fiat currencies
* [GDAX](https://www.gdax.com/) - digital asset exchange with trading API

# Online Banks / Online Money Transfers
* [N26](https://n26.com/) - German direct bank
* [PayPal](https://www.paypal.com) - internet payment companies
* [Revolut](https://www.revolut.com/) - pre-paid debit card (MasterCard), currency exchange, and peer-to-peer payments

# Pensions & Retirement / Saving & Investments / Life Insurrance
* [Amundi](https://www.amundi.ie/) - savings, investments, pensions
* [Bank of Ireland](https://www.bankofireland.com/) - savings, investments, pensions, insurance (home, car, travel, life)
* [Davy Select](http://www.davyselect.ie/) - investments (shares, investments funds, ETFs, bonds), pensions
* [Degiro](https://www.degiro.ie/) - investments (shares, investments funds, ETFs, futures, options, rights, CFD)
* [Deziro](https://www.deziro.com/ie/) - stock trading
* [Irish Life](https://www.irishlife.ie/) - pensions, investments, life insurance, health insurance
* [iShares](https://www.ishares.com) - investments
* [Xetra](http://www.xetra.com/) - shares, ETFs, actve ETFs, ETCs, ETNs
* [Zurich Life Assurance](http://zurichlife.ie/) - all / investment funds

# Financial Tools / Apps
* [Buddi](http://buddi.digitalcave.ca/) - personal finance and budgeting program
* [Financial Calculators](http://www.calculator.net/financial-calculator.html) - various financial calculators (mortgage and real estate, auto, investment, retirement, tax and salary, loans, ...)
* [Fund Lite - IrishTimes](http://funds.irishtimes.com/) - list of funds
* [GnuCash](http://www.gnucash.org/) - personal and small-business financial-accounting software
* [Google Finance](https://www.google.com/finance) - financial information site, portfolio management
* [justETF.com ETF Screener](https://www.justetf.com/uk/find-etf.html?replicationType=replicationType-full&replicationType=replicationType-sampling&ls=any&dc=IE)
* [justETF](https://www.justetf.com) - online investing software for portfolio management
* [KMyMoney](http://www.kmymoney.org) - personal finance manager
* [Moneydance](http://moneydance.com/) - personal finance software
* [Morningstar.co.uk ETF Screener](http://tools.morningstar.co.uk/uk/etfscreener/default.aspx)
* [Personal Capital](https://www.personalcapital.com/financial-software) - net worth, fee analyzer, investment checkup, retirement planner
* [Reddit Personal Finance Tools](https://www.reddit.com/r/personalfinance/wiki/tools) - list of tools
* [Sharesight](https://www.sharesight.com/) - stock portfolio tracker
* [Skrooge](https://skrooge.org/) - personal finance manager
* [ValueSignals](https://www.valuesignals.com/) - stock market screener
* [Yahoo Finance](https://finance.yahoo.com/) - financial information site, portfolio management

# APIs
* [Alpha Vantage](https://www.alphavantage.co/) - realtime and historical equity data
* [Open Exchange Rates](https://openexchangerates.org/) - historical currency rates
* [OpenFIGI](https://openfigi.com/) - open system for identifying instruments globally across all asset classes

# Courses
* [CS007](https://cs007.blog/) - personal finance for engineers

# Definitions
* [Bond](https://en.wikipedia.org/wiki/Bond_%28finance%29)
* [ETF](https://en.wikipedia.org/wiki/Exchange-traded_fund)
* [Index Fund](https://en.wikipedia.org/wiki/Index_fund)
* [Mutual Fund](https://en.wikipedia.org/wiki/Mutual_fund)
* [Stock](https://en.wikipedia.org/wiki/Stock)

