module Jekyll
  class BuildInformationGenerator < Generator

    def generate(site)
      build_information = {}
      build_information['site_version']=site_version()
      build_information['jekyll_version']=jekyll_version()

      site.config['build_information']=build_information
    end

    private

    def site_version()
      (ENV['JEKYLL_418L_VERSION'].nil?) ? "##unknown##" : ENV['JEKYLL_418L_VERSION']
    end

    def jekyll_version()
      "#{Jekyll::VERSION}"
    end

  end
end
