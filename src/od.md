---
layout: page
title: Open Data
order: 195
sections:
  - menu
permalink: /od/
---

# APIs
* [Exchange Rate API](https://www.exchangerate-api.com/) - exchange rate API
* [Open Exchange Rates](https://openexchangerates.org/) - historical currency rates
* [fixer.io](https://fixer.io/) - foreign exchange rates and currency conversion API

# Geolocation
* [GeoLite2 Free](https://dev.maxmind.com/geoip/geoip2/geolite2/) - free IP geolocation databases
* [Maxmind](https://www.maxmind.com/en/open-source-data-and-api-for-ip-geolocation) - Geolite database
* [ip2location](http://lite.ip2location.com/)

# Government's open data
* [DATA.GOV.HR](https://data.gov.hr/) - Croatian's open data portal
* [DATA.GOV.IE](https://data.gov.ie/data) - Ireland's open data portal
* [DATA.GOV](https://www.data.gov/) - The home of the U.S. Government’s open data
* [European data portal](https://www.europeandataportal.eu/) - European Union's open data

# NASA
* [NASA Open Data](https://open.nasa.gov/open-data/)
* [NASA's Open Data Portal](https://data.nasa.gov/)

# Other
* [World Bank Open Data](http://data.worldbank.org/) - Free and open access to global development data
* [Kaggle Datasets](https://www.kaggle.com/datasets) - Kaggle Datasets
