.PHONY: all build clean distclean serve dpkg

all: build

build:
	bundle exec jekyll build -s src/

install:
	mkdir -p $(DESTDIR)/usr/share/doc/418l.pw/www
	cp -r _site/* $(DESTDIR)/usr/share/doc/418l.pw/www/

serve:
	bundle exec jekyll serve -s src/

clean:
	rm -rf .sass-cache _site
	rm -rf debian/418l.pw.debhelper.log
	rm -rf debian/418l.pw.substvars
	rm -rf debian/418l.pw/
	rm -rf debian/debhelper-build-stamp
	rm -rf debian/files

distclean: clean

dpkg:
	debuild --prepend-path /usr/local/bin --preserve-envvar=JEKYLL_418L_VERSION -i -us -uc -b

